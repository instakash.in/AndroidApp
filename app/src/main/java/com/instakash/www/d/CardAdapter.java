package com.instakash.www.d;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by vaibhavjain on 02/09/16.
 */

public class CardAdapter extends RecyclerView.Adapter<RVViewHolder> {
    Context c;
    String[] forms;
    public CardAdapter(Context c, String[] forms) {
        this.c = c;
        this.forms = forms;
    }
    @Override
    public RVViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.formcard, parent, false);
        return new RVViewHolder(v);
    }
    @Override
    public void onBindViewHolder(RVViewHolder holder, int position) {
        //BIND DATA
        holder.nameTxt.setText(forms[position]);
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                if(pos==3){
                    Intent i = new Intent(c,PersonalInfoForm1.class);
                    c.startActivity(i);



                }

            }
        });
    }
    @Override
    public int getItemCount() {
        return forms.length;
    }


}