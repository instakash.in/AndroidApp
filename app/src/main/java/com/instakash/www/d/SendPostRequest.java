package com.instakash.www.d;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class SendPostRequest extends AsyncTask<String, Void, String> {
    public AsyncResponse delegate = null;

        @Override
        protected String doInBackground(String... params) {
            try {

                Log.i("paa",params[0]);
                Log.i("paa",params[1]);
                Log.i("paa1",params.toString());
                URL url = new URL(params[1]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(150000);
                conn.setConnectTimeout(150000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(PPUtlilities.getPostDataString( new JSONObject (params[0])));


                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();
                Log.i("resp", String.valueOf(responseCode));
                Log.i("resp", String.valueOf(HttpURLConnection.HTTP_OK));
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
//                    in.close();
                    Log.i("res",sb.toString());
//                    return sb.toString();
                    return String.valueOf(responseCode);
                }
                else {
                    return new String("false : "+responseCode);
                }
            }

            catch(Exception e){
                Log.i("res",e.toString());
//                returnx new String("Exception: " + e.getMessage());
                return "cool";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("res",result);
            delegate.processFinish(result);


        }


//        public String getPostDataString(JSONObject params) throws Exception {
//
//            StringBuilder result = new StringBuilder();
//            boolean first = true;
//            Iterator<String> itr = params.keys();
//            while(itr.hasNext()){
//                String key= itr.next();
//                Object value = params.get(key);
//                if (first)
//                    first = false;
//                else
//                    result.append("&");
//
//                result.append(URLEncoder.encode(key, "UTF-8"));
//                result.append("=");
//                result.append(URLEncoder.encode(value.toString(), "UTF-8"));
//
//            }
//
//            Log.i("Wala",result.toString());
//            return result.toString();
//        }
    }

