package com.instakash.www.d;

/**
 * Created by vaibhavjain on 02/09/16.
 */
public class CardData {
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public CardData(String title) {
        this.title = title;
    }
}
