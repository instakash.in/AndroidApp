package com.instakash.www.d;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentActivity extends AppCompatActivity {
    TextView summaryText;
    ListView paymentListView;
    String[] paymentMethods = new String[] { "Card Payment",
            "Cash",
            "PostPay"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        summaryText = (TextView) findViewById(R.id.summarayText);
        final String summary = getIntent().getExtras().getString("totalBill");
        summaryText.setText(getResources().getString(R.string.Rs)+ " " +summary);
        paymentListView = (ListView) findViewById(R.id.paymemtListView);

        ArrayAdapter paymentListViewAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,paymentMethods);
        paymentListView.setAdapter(paymentListViewAdapter);

        paymentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) paymentListView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                        .show();

                if(itemPosition==0){
                    Intent cardPaymentIntent = new Intent(PaymentActivity.this,CardPaymentActivity.class);
                    Log.i("wala s",String.valueOf(summary));
                    cardPaymentIntent.putExtra("totalBill", summary);
                    startActivity(cardPaymentIntent);
                }
                else if(itemPosition==1){
                    Intent cardPaymentIntent = new Intent(PaymentActivity.this,CashPaymentActivity.class);
                    startActivity(cardPaymentIntent);
                }
                else if(itemPosition==2){
                    Intent cardPaymentIntent = new Intent(PaymentActivity.this,CardPaymentActivity.class);
                    startActivity(cardPaymentIntent);
                }
            }


        });

    }

}
