package com.instakash.www.d;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by vaibhavjain on 03/09/16.
 */
public class RVViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView nameTxt;
    ItemClickListener itemClickListener;
    public RVViewHolder(View itemView) {
        super(itemView);
        nameTxt = (TextView) itemView.findViewById(R.id.nameTxt);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(this.getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener=itemClickListener;
    }
}
