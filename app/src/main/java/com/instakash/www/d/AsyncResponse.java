package com.instakash.www.d;


import org.json.JSONObject;

public interface AsyncResponse {
    void processFinish(String output);
}