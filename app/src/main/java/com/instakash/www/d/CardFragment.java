package com.instakash.www.d;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;


/**
 * Created by vaibhavjain on 01/09/16.
 */
public class CardFragment extends Fragment {
    private SlidingUpPanelLayout mLayout;
    int decider = 2;
    RecyclerView recyclerView;
    ListView lv;
    ArrayAdapter arrayAdapter;
    CarouselView carouselView;
    ImageListener imageListener;

    int[] sampleImages = {R.mipmap.ic_launcher};
    String[] formType={"Socail Logins","Invite Friends","Other Info","Personal Info","Work Details","Click a Selfie","Proof of Indentity","Proof of Address","Bank Statement","Salary Slip"};
    RecyclerView rv;
    ViewPager viewPager;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        super.onCreateView(inflater, container, savedInstanceState);
        View v =  inflater.inflate(R.layout.fragment_card, container, false);
        rv = (RecyclerView) v.findViewById(R.id.recycler_view);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        //ADAPTER
        CardAdapter adapter = new CardAdapter(this.getActivity(), formType);
        rv.setAdapter(adapter);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        viewPager.setAdapter(new pagerAdapter(fragmentManager));
        mLayout = (SlidingUpPanelLayout) v.findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i("slide", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i("slide", "onPanelStateChanged " + newState);
            }
        });




        return v;

    }

    private class pagerAdapter extends FragmentStatePagerAdapter{
        Fragment fragment = null;

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position==0)
            fragment = new RBLFragment();
            if(position==1 && decider==2) fragment = new PayTMFragment();

            return fragment;
        }

        @Override
        public int getCount() {
            return decider;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position==0)
            {
                return "RBL Bank CC";
            }

            if(position==1 && decider ==2){
                return "PayTM";
            }

            return null;
        }
    }

}

