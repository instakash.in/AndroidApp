package com.instakash.www.d;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

public class HomeScreenActivity extends AppCompatActivity implements AHBottomNavigation.OnTabSelectedListener{
    AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnTabSelectedListener(this);
        this.createNavItem();

    }

    private void createNavItem(){
        AHBottomNavigationItem cardItem = new AHBottomNavigationItem("Cards",R.drawable.ic_local_dining_white_24dp);
        AHBottomNavigationItem paymentItem = new AHBottomNavigationItem("Pats",R.drawable.ic_favorite_white_24dp);
        AHBottomNavigationItem chatItem = new AHBottomNavigationItem("Chat",R.drawable.ic_location_on_white_24dp);
        AHBottomNavigationItem profileItem = new AHBottomNavigationItem("Profile",R.drawable.ic_favorite_white_24dp);

        bottomNavigation.addItem(paymentItem);
        bottomNavigation.addItem(cardItem);
        bottomNavigation.addItem(chatItem);
        bottomNavigation.addItem(profileItem);

        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#fefefe"));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

// Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

// Force the titles to be displayed (against Material Design guidelines!)
        bottomNavigation.setForceTitlesDisplay(true);

// Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        if (position==0){
            PaymentFragment paymentFragment = new PaymentFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.homescreen,paymentFragment).commit();
        }
        else if (position==1){
            CardFragment cardFragment = new CardFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.homescreen,cardFragment).commit();
        }
        else if (position==2){
            ChatFragment chatFragment = new ChatFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.homescreen,chatFragment).commit();
        }
        else if (position==3){
            ProfileFragment profileFragment = new ProfileFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.homescreen,profileFragment).commit();
        }
        return true;
    }


}
