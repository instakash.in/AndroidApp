package com.instakash.www.d;

/**
 * Created by vaibhavjain on 03/09/16.
 */
public interface ItemClickListener {
    void onItemClick(int pos);
}
