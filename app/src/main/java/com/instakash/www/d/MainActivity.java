package com.instakash.www.d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button totalDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        Button one = (Button) findViewById(R.id.btnOne);
        one.setOnClickListener(this);
        Button two = (Button) findViewById(R.id.btnTwo);
        two.setOnClickListener(this);
        Button three = (Button) findViewById(R.id.btnThree);
        three.setOnClickListener(this);
        Button four = (Button) findViewById(R.id.btnFour);
        four.setOnClickListener(this);
        Button five = (Button) findViewById(R.id.btnFive);
        five.setOnClickListener(this);
        Button six = (Button) findViewById(R.id.btnSix);
        six.setOnClickListener(this);
        Button seven = (Button) findViewById(R.id.btnSeven);
        seven.setOnClickListener(this);
        Button eight = (Button) findViewById(R.id.btnEight);
        eight.setOnClickListener(this);
        Button nine = (Button) findViewById(R.id.btnNine);
        nine.setOnClickListener(this);
        Button cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(this);
        Button add = (Button) findViewById(R.id.btnAdd);
        add.setOnClickListener(this);


    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onClick(View v) {
        Button b = (Button) v;
        TextView textbox = (TextView) findViewById(R.id.thetextbox);
        String textOnTextView = (String) textbox.getText();
        Log.i("init",textOnTextView);
        String textByButton = (String) b.getText();
        totalDisplay = (Button) findViewById(R.id.totaldisplay);
        if(v.getId()==R.id.btnCancel) {
            if(textOnTextView.length() != 0)
            textbox.setText(textOnTextView.substring(0,textOnTextView.length()-1));
        }
        else if (v.getId()==R.id.btnAdd){
            String s = (String) totalDisplay.getText();
            Log.i("wola the string",s);
            Integer totalDisplayInt = Integer.parseInt(s);
            Integer i1 = Integer.parseInt(textOnTextView.replace("\u20B9",""));
            int sum = totalDisplayInt+i1;
            totalDisplay.setText(String.valueOf(sum));
            textbox.setText(getResources().getString(R.string.Rs));
        }
        else {
            String textWithoutRs = textOnTextView.replace("\u20B9","");
            Log.i("wala without",textWithoutRs);
            String concatText = textWithoutRs+textByButton;
            Log.i("wala concat",concatText) ;
            String finalText = getResources().getString(R.string.Rs)+""+concatText;
            Log.i("wala final",finalText);
            textbox.setText(finalText);



        }

    }

    public void makeTransaction(View v){
        Intent i = new Intent(this,PaymentActivity.class);
        String totalAmount = (String) totalDisplay.getText();
        i.putExtra("totalBill",totalAmount);
        startActivity(i);
    }
}


