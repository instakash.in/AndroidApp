package com.instakash.www.d;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class OTPActivity extends AppCompatActivity implements View.OnClickListener,AsyncResponse {
     EditText edt;
     Button sendOTPBtn;
    SendPostRequest spr = new SendPostRequest();
    SharedPreferences sharedpreferences;
    UserSessionManager session;
    public static final String MyPREFERENCES = "MyPrefs" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        session = new UserSessionManager(getApplicationContext());
        spr.delegate = this;

        edt = (EditText) findViewById(R.id.activity_otp_phoneNumberET);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        sendOTPBtn  = (Button) findViewById(R.id.activity_otp_sendOTPBtn);
        edt = (EditText) findViewById(R.id.activity_otp_phoneNumberET);

        sendOTPBtn.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        String phoneText = edt.getText().toString();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("phone",phoneText);
        editor.commit();
        try {
//            spr = new SendPostRequest();
            JSONObject params = new JSONObject();
            params.put("phone",phoneText);
//            spr.execute(params.toString(),"http://192.168.1.100:5000/api/mobileotp");
//            spr.execute(params.toString(),"http://192.168.43.40:5000/api/mobileotp");
            Intent i = new Intent(this,HomeScreenActivity.class);
            startActivity(i);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void processFinish(String output) {
        Log.i("process1",output);


    }


//    private class SendReq extends AsyncTask<String, Void, String>{
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//
//                Log.i("paa",params[0]);
//                Log.i("paa",params[1]);
//                Log.i("paa1",params.toString());
//                URL url = new URL(params[1]);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setReadTimeout(150000);
//                conn.setConnectTimeout(150000);
//                conn.setRequestMethod("POST");
//                conn.setDoInput(true);
//                conn.setDoOutput(true);
//                OutputStream os = conn.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
//                writer.write(PPUtlilities.getPostDataString( new JSONObject (params[0])));
//
//
//                writer.flush();
//                writer.close();
//                os.close();
//                Log.i("resp","beofer getting code");
//                int responseCode=conn.getResponseCode();
//                Log.i("resp", String.valueOf(responseCode));
//                Log.i("resp", String.valueOf(HttpURLConnection.HTTP_OK));
//                if (responseCode == HttpsURLConnection.HTTP_OK) {
//                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//                    StringBuffer sb = new StringBuffer("");
//                    String line="";
//                    while((line = in.readLine()) != null) {
//                        sb.append(line);
//                        break;
//                    }
////                    in.close();
//                    Log.i("res",sb.toString());
//                    return sb.toString();
//                }
//                else {
//                    return new String("false : "+responseCode);
//                }
//            }
//
//            catch(Exception e){
//                Log.i("res",e.toString());
////                returnx new String("Exception: " + e.getMessage());
//                return "cool";
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String result){
//            Log.i("res",result);
//        }
//    }
}
