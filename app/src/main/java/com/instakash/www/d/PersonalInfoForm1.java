package com.instakash.www.d;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PersonalInfoForm1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info_form1);

    }

    public void nextScreen(View view) {
        Intent i = new Intent(this,PersonalInfoForm2.class);
        startActivity(i);
    }
}
