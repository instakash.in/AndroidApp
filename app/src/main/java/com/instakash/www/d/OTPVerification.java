package com.instakash.www.d;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class OTPVerification extends AppCompatActivity implements AsyncResponse{
    EditText et;
    Button btn;
    SendPostRequest spr1 = new SendPostRequest();
    SharedPreferences sharedpreferences;
    UserSessionManager session;
    public static final String MyPREFERENCES = "MyPrefs" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);
        spr1.delegate = this;
        //// TODO: 01/09/16 make one shared pref name.myprefs and AndroidExamplePref..wtf
        session = new UserSessionManager(getApplicationContext());
        et = (EditText) findViewById(R.id.activity_otpverification_edit);
        btn = (Button)findViewById(R.id.activity_otpverification_btn);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 01/09/16 check empty otp 
                String text = et.getText().toString();
                Log.i("haha",text);
//                spr1 = new SendPostRequest();
                JSONObject params = new JSONObject();
                try {
                    String a = sharedpreferences.getString("phone","null");
                    Log.i("haha",a);
                    params.put("phone",sharedpreferences.getString("phone","null"));
                    params.put("otp",text);
                    Log.i("haha",spr1.getStatus().toString());
                    spr1.execute(params.toString(),"http://192.168.43.40:5000/api/verifyotp");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void processFinish(String output) {
        Log.i("process123",output.getClass().getName());
        if(output.equals("200")){
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putString("otp",et.getText().toString());
//            editor.commit();
            session.createUserLoginSession(sharedpreferences.getString("phone","null"),
                    et.getText().toString());
            Intent i = new Intent(this, HomeScreenActivity.class);
            startActivity(i);
        }
        else{
            Toast.makeText(getApplicationContext(), "Wrong OTP",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private class SendReq extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {

                Log.i("paa",params[0]);
                Log.i("paa",params[1]);
                Log.i("paa1",params.toString());
                URL url = new URL(params[1]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(150000);
                conn.setConnectTimeout(150000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(PPUtlilities.getPostDataString( new JSONObject (params[0])));


                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();
                Log.i("resp", String.valueOf(responseCode));
                Log.i("resp", String.valueOf(HttpURLConnection.HTTP_OK));
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
//                    in.close();
                    Log.i("res",sb.toString());
                    return String.valueOf(responseCode);
                }
                else {
                    return new String("false : "+responseCode);
                }
            }

            catch(Exception e){
                Log.i("res",e.toString());
//                returnx new String("Exception: " + e.getMessage());
                return "cool";
            }
        }

        @Override
        protected void onPostExecute(String result){
            Log.i("res",result);
        }
    }
}
